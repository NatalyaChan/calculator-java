import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class TCalculator {


  private JPanel panelMain;
  private JButton button1;
  private JLabel labelDisplay;
  private JLabel labelExpression;
  private JButton button2;
  private JButton button3;
  private JButton button4;
  private JButton button5;
  private JButton button6;
  private JButton button7;
  private JButton button8;
  private JButton button9;
  private JButton button0;
  private JButton buttonDivision;
  private JButton buttonMultiplication;
  private JButton buttonMinus;
  private JButton buttonPlus;
  private JButton buttonResult;
  private JButton buttonPoint;
  private JButton buttonSquare;
  private JButton buttonReverse;
  private JButton buttonCE;
  private JButton buttonClear;
  private JButton buttonPercent;
  private JButton buttonSign;
  private JButton buttonSquareRoot;

  private double X=0;
  private boolean flag=false;
  private boolean flagFinish=false;
  private boolean withPoint=false;
  private String operation="";
  private String prevLabelExpression="";

  public TCalculator() {
    button1.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        flagOn();
        checkNewExpression();
        labelDisplay.setText(labelDisplay.getText()+"1");
      }
    });

    button2.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        flagOn();
        checkNewExpression();
        labelDisplay.setText(labelDisplay.getText()+"2");
      }
    });

    button3.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        flagOn();
        checkNewExpression();
        labelDisplay.setText(labelDisplay.getText()+"3");
      }
    });

    button4.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        flagOn();
        checkNewExpression();
        labelDisplay.setText(labelDisplay.getText()+"4");
      }
    });

    button5.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        flagOn();
        checkNewExpression();
        labelDisplay.setText(labelDisplay.getText()+"5");
      }
    });

    button6.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        flagOn();
        checkNewExpression();
        labelDisplay.setText(labelDisplay.getText()+"6");
      }
    });

    button7.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        flagOn();
        checkNewExpression();
        labelDisplay.setText(labelDisplay.getText()+"7");
      }
    });

    button8.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        flagOn();
        checkNewExpression();
        labelDisplay.setText(labelDisplay.getText()+"8");
      }
    });

    button9.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        flagOn();
        checkNewExpression();
        labelDisplay.setText(labelDisplay.getText()+"9");
      }
    });

    button0.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        flagOn();
        checkNewExpression();
        labelDisplay.setText(labelDisplay.getText()+"0");
      }
    });

    buttonPlus.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        operationInProgress();
        operation="+";
        addOperationToExpression();
        flagOff();
      }
    });

    buttonMinus.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        operationInProgress();
        operation="-";
        addOperationToExpression();
        flagOff();
      }
    });

    buttonMultiplication.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        operationInProgress();
        operation="*";
        addOperationToExpression();
        flagOff();
      }
    });

    buttonDivision.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        operationInProgress();
        operation="/";
        addOperationToExpression();
        flagOff();
      }
    });

    buttonResult.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        calculateExpression();
        labelExpression.setText("");
        prevLabelExpression=labelDisplay.getText();
        flagOff();
        flagFinish=true;
      }
    });

    buttonPoint.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        checkNewExpression();
        if((flag) && (!labelDisplay.getText().contains("."))) {
          withPoint=true;
          labelDisplay.setText(labelDisplay.getText()+'.');
        }
      }
    });

    buttonSign.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        checkNewExpression();
        labelDisplay.setText(convertToString(convertToDouble(labelDisplay.getText())*(-1)));
        if(!flag) {
          labelExpression.setText("("+prevLabelExpression+")"+"*(-1)");
        }
        prevLabelExpression=labelExpression.getText();
      }
    });

    buttonSquare.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        operationInProgress();
        operation="^2";
        labelDisplay.setText(convertToString(Math.pow(convertToDouble(labelDisplay.getText()), 2)));
        if(flag) {
          labelExpression.setText("sqr("+labelExpression.getText()+")");
        } else {
          labelExpression.setText("sqr("+prevLabelExpression+")");
        }
        prevLabelExpression=labelExpression.getText();
        flagOff();
      }
    });

    buttonSquareRoot.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        operationInProgress();
        operation="sqrt";
        if(convertToDouble(labelDisplay.getText())<0) {
          labelExpression.setText("Этот калькулятор не работает с комплексными числами");
        } else {
          labelDisplay.setText(convertToString(Math.sqrt(convertToDouble(labelDisplay.getText()))));
          if(flag) {
            labelExpression.setText("sqrt("+labelExpression.getText()+")");
          } else {
            labelExpression.setText("sqrt("+prevLabelExpression+")");
          }
          prevLabelExpression=labelExpression.getText();
          flagOff();
        }
      }
    });

    buttonReverse.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        operationInProgress();
        operation="1/";
        if(convertToDouble(labelDisplay.getText())==0) {
          labelDisplay.setText("Зачем вы делите на 0?");
        } else {
          labelDisplay.setText(convertToString(1/convertToDouble(labelDisplay.getText())));
          if(flag) {
            labelExpression.setText("1/("+labelExpression.getText()+")");
          } else {
            labelExpression.setText("1/("+prevLabelExpression+")");
          }
          prevLabelExpression=labelExpression.getText();
          flagOff();
        }
      }
    });

    buttonPercent.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        labelDisplay.setText(convertToString(X*convertToDouble(labelDisplay.getText())/100));
      }
    });

    buttonClear.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        X=0;
        flag=false;
        flagFinish=false;
        withPoint=false;
        operation="";
        prevLabelExpression="";
        labelDisplay.setText("0");
        labelExpression.setText("");
      }
    });

    buttonCE.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        labelDisplay.setText("0");
        flag=false;
      }
    });
  }

  public void flagOff() {  // рычаг опущен. Необработанных чисел нет, результат есть
    flag=false;
  }


  public void flagOn() {  //рычаг активирован, экран очищен, в него введено новое необработанное число
    if((!flag) && (!withPoint)) {
      labelDisplay.setText("");
    }
    flag=true;
  }

  public void showCurrentResult() {
    if(flag) {
      calculateExpression();
    }
  }

  public void checkNewExpression() {
    if(flagFinish) {
      flag=true;
      flagFinish=false;
    }
  }


  public void addOperationToExpression() {
    if(flag) {
      prevLabelExpression=labelExpression.getText();
      labelExpression.setText(bracketer(labelExpression.getText()));
    } else {
      labelExpression.setText(bracketer(prevLabelExpression));
    }
  }


  public String bracketer(String expression) {
    String S="";
    if(((operation=="*") || (operation=="/")) &&
        ((prevLabelExpression.contains("+")) || (prevLabelExpression.contains("-")))) {
      S="("+expression+")"+operation;
    } else {
      S=expression+operation;
    }
    return S;
  }


  public void addDisplaytoExpression() {
    if(convertToDouble(labelDisplay.getText())<0) {
      labelExpression.setText(labelExpression.getText()+"("+labelDisplay.getText()+")");
    } else {
      labelExpression.setText(labelExpression.getText()+labelDisplay.getText());
    }
  }

  public String convertToString(double X) {
    int Y=0;
    if(X==Math.floor(X)) {
      Y=(int) Math.floor(X);
      return (""+Y);
    } else {
      return (""+X);
    }
  }

  public Double convertToDouble(String X) {
    double D=0;
    try {
      D=Double.parseDouble(X);
    } catch(NumberFormatException e) {
      System.err.println("Неверный формат строки!");
    }
    return D;
  }


  public void operationInProgress() {
    checkNewExpression();
    if(flag) {
      addDisplaytoExpression();
    }
    showCurrentResult();
    if(labelDisplay.getText()!="Зачем вы делите на 0?") {
      try {
        X=convertToDouble(labelDisplay.getText());
      } catch(NumberFormatException e) {
        System.err.println("Неверный формат строки!");
      }
    }
  }

  public void calculateExpression() {
    withPoint=false;
    if(!flag) {
      labelExpression.setText(prevLabelExpression);
    } else {
      Double D=convertToDouble(labelDisplay.getText());
      switch(operation) {
        case "+": {
          D=X+D;
          break;
        }
        case "-": {
          D=X-D;
          break;
        }
        case "*": {
          D=X*D;
          break;
        }
        case "/": {
          if(D==0) {
            labelDisplay.setText("Зачем вы делите на 0?");
          } else {
            D=X/D;
          }
          break;
        }
      }
      if((operation=="^2") || (operation=="sqrt") || (operation=="1/")) {
        prevLabelExpression=labelExpression.getText();
      }
      operation="";
      labelDisplay.setText(convertToString(D));
    }
  }

  public static void main(String[] args) {
    TCalculator calculator=new TCalculator();
    JFrame frame=new JFrame("Calculator");
    frame.setContentPane(calculator.panelMain);
    frame.setSize(330, 300);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setLocationRelativeTo(null);
    //   frame.pack();
    frame.setVisible(true);
  }
}
